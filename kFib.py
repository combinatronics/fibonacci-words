from sys import argv

def kFibWords(k, n): 
  # Check args and handle base cases.
  assert k >= 2 and n >= 0
  if n == 0 or n == 1:
    yield () if n == 0 else (1,) * k
    return

  # Expand words over [n-2] and [n-1].
  p = (n,)*(k-1) + (n-1,)*k + (n,)
  for w in kFibWords(k, n-2):
    yield p + w
  p = (n,)*(k-1)
  for w in kFibWords(k, n-1):
    for i in range(k):
      yield p + w[:i] + (n,) + w[i:]

if __name__ == "__main__":
  # Arguments and usage information.
  name = argv[0]
  numArgs = len(argv)
  if numArgs == 3:
    k = int(argv[1])
    n = int(argv[2])
  if numArgs != 3 or k < 2 or n < 0:
    print("python3 " + name + " k n "\
      + "with k >= 2 and n >= 0 for "\
      + "k-Fibonacci words over [n].")
    exit(0)

  # Generate and print the words.
  total = 0
  for word in kFibWords(k, n):
    print(*word, sep="")
    total += 1
  print("total: %d" % total)
