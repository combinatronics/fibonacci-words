from sys import argv

# Generates cool-lex order for the permutations of a multiset given as a list.
def coolMulti(multiset):
    n = len(multiset)
    word = sorted(multiset, reverse = True)
    inc = n-2
    while inc != n-1:
        yield word
        i = inc+1 if inc == len(word)-2 or word[inc] < word[inc+2] else inc+2
        value = word.pop(i)
        word.insert(0, value)
        inc = 0 if word[0] < word[1] else inc+1

# Generates the cool-lex order of k-regular words over [n].
def coolRegular(k, n):
    multiset = [i for j in range(k) for i in range(1,n+1)]
    yield from coolMulti(multiset)
    
if __name__ == "__main__":
    # Process arguments (and provide usage information).
    k = int(argv[1]) if len(argv) >= 2 else None
    n = int(argv[2]) if len(argv) >= 3 else None
    if len(argv) != 3 or k < 1 or n < 1 or k*n == 1:
        print("python3 " + argv[0] + " k n with k >= 1 and n >= 1 and kn > 1")
        exit(0)

    # Generate and print the k-regular words over [n].
    total = 0
    for word in coolRegular(k, n):
        print(*word, sep="")
        total += 1
    print("total %d" % total)
