# fibonacci-words

These Python files can be used to generate Fibonacci-$k$ and $k$-Fibonacci words over $[n] = {1,2,...,n}$ using the paper *Pattern Avoidance for two k-ary Fibonacci Sequences using k-Regular Words* by Downing, Hartung, and Williams.
- A Fibonacci-$k$ word with $k \geq 1$ is a $k$-regular word that avoids the patterns $121, 123, 132, 213$.
  They are counted by the Fibonacci-$k$ numbers: $a_k(n) = a_k(n-1) + k \cdot a_k(n-2)$ with $a_k(0) = a_k(1) = 1$.
- A $k$-Fibonacci word with $k \geq 2$ is a $k$-regular word that avoids the patterns $122, 213$.
  They are counted by the Fibonacci-$k$ numbers: $b_k(n) = k \cdot b_k(n-1) + b_k(n-2)$ with $b_k(0) = b_k(1) = 1$.

For example, run ```python3 Fibk.py 3 4``` and ```python3 kFib.py 3 4``` to generate the words with $k=3$ and $n=4$ in Figure 1 of the aforementioned paper.

In addition, a utility for generating $k$-regular words (or more generally, multiset permutations) in cool-lex order is provided.
For example, run ```python3 coolRegular.py 3 4``` to generate all k-regular words with $k=3$ and $n=4$ .
This utility is from the paper *Loopless generation of multiset permutations using a constant number of variables by prefix shifts* by Williams.
