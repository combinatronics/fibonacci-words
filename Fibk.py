from sys import argv

def FibkWords(k, n): 
  # Check args and handle base cases.
  assert k >= 1 and n >= 0
  if n == 0 or n == 1:
    yield () if n == 0 else (1,) * k
    return

  # Expand words over [n-2] and [n-1].
  for i in range(k):
    p = (n,)*i + (n-1,)*k + (n,)*(k-i)
    for w in FibkWords(k, n-2):
      yield p + w
  p = (n,)*k
  for w in FibkWords(k, n-1):
    yield p + w

if __name__ == "__main__":
  # Arguments and usage information.
  name = argv[0]
  numArgs = len(argv)
  if numArgs == 3:
    k = int(argv[1])
    n = int(argv[2])
  if numArgs != 3 or k < 1 or n < 0:
    print("python3 " + name + " k n "\
      + "with k >= 1 and n >= 0 for "\
      + "Fibonacci-k words over [n].")
    exit(0)

  # Generate and print the words.
  total = 0
  for word in FibkWords(k, n):
    print(*word, sep="")
    total += 1
  print("total: %d" % total)
